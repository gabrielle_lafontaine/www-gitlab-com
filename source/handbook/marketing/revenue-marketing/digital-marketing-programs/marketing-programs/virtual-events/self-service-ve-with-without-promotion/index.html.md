---
layout: markdown_page
title: "Self-service virtual events with or without promotion"
---

## Self-service virtual events with or without promotion

This section focuses on best practices and logistical set up for self-service virtual events with or without promotion.

### Project Planning

Once you have an idea and abstract for a self-service virtual event with or without promotion immediately:

1.  Create the [self-service virtual event issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=meeting-with-without-breakout) and assign to yourself.
2.  Follow the steps outlined in the issue template.

### Logistical Set up

#### Setting up the event in Zoom

##### Prerequisites

Make sure these settings are enabled in your personal zoom account settings as needed. In your zoom web interface go to `Settings`.

1.  To prevent meeting disruptions when participants join and leave by making sure:
     * Under In Meeting (Basic section), `Play sound when participants join or leave` is deactivated
     * Toggling `Mute participants upon entry` to active.
2. To enable Livestream feature:
    *  Under In Meeting (Advanced) section, toggle `Allow live streaming the meetings and enable Youtube` to active
3. To enable Polling feature:
    *  Under In Meeting (Basic) section, toggle `Polling` to active.
4. To enable Breakout sessions feature:
    *  Under In Meeting (Advanced) section, toggle `Breakout room` to active.
    *  To enable pre-assigning for zoom users, check  `Allow host to assign participants to breakout rooms when scheduling`.
5. Optional but [useful best practice set up from Zoom](https://blog.zoom.us/wordpress/2020/03/20/keep-uninvited-guests-out-of-your-zoom-event/) to prevent [Zoom Bombing](https://www.forbes.com/sites/kateoflahertyuk/2020/03/27/beware-zoom-users-heres-how-people-can-zoom-bomb-your-chat/#26745c14618e) (hackers hijacking your meeting).

**Creating the event in Zoom**

1. Login to zoom via the web interface. 
2. Click `schedule a meeting` on the top right navigation.
3. Fill in the `Topic` in the recommended format (this will be in the subject line for confirmation emails): 
     * For One-time (non-recurring) event: `Event title` - DD Month, 2020 - Local time (e.g: Deploy AWS Lambda applications with ease - April 9, 2020, 9 am PT). 
     * For Recurring event: `Event title` (e.g: Deploy AWS Lambda applications with ease). 

4. Fill in the `Description` with a description of what the event is about. *Note: this will be in the body for confirmation emails and landing page (if you set registration as required in step 9).
5. Fill in `When` with the event date and time.
6. Fill in `Duration` with the duration of the event.
7. Select the  `Timezone` where the event will be held.
8. (Optional) If your event is going to be recurring, click recurring meetings and set up `Recurrence` and specify the `End date` of the recurring event.
9. For events where you will need to track registration and attendance, make sure you check the `Registration` field as `Required`. Doing this will automatically trigger a landing page to be created for your event.
10.(Optional) If you set the event as recurring in step 8, specify which registration process will work best for your target audience:
     * Attendees register once and can attend any of the occurrences - This will automatically register the registrants to ALL sessions upon form fill.
     * Attendees need to register for each occurrence to attend - This will allow registrants to only select ONE session per form fill.
     * Attendees register once and can choose one or more occurrences to attend - Registrants are presented with checkboxes to select which session they want to attend on the landing page prior to filling out the form.

11. (Optional) You can make the meeting private/ensure only those that registered attend by checking the `Require a meeting password` option  and including the auto generated password in your invite (for events not requiring registration)/confirmation (for events requiring registration). This is useful so only the people you intend to invite attends the meeting and can help prevent hackers from taking over your meeting (Zoom Bombing).
12. Specify if you want the Host and/or Participant’s video `on` or `off`.
13. Make sure audio is set to `BOTH` telephone and computer audio.
14. For meeting options configurations:
15. Check “Enable join before host` option if you want to allow participants to join before the host(s). 
16. **(Recommended)** Check `Mute participants upon entry` to prevent disruption whenever someone joins mid-presentation. 
17. **(Recommended)** Check the `Enable waiting room` option if you’d like to choose some participants to join prior to others . This is useful to allow a final dry run between host and the other presenters right before the event. Activating this is recommended by zoom to prevent [Zoom Bombing](https://blog.zoom.us/wordpress/2020/03/20/keep-uninvited-guests-out-of-your-zoom-event/).
18. Check `Only authenticated users can join` to restrict meeting only to participants that have logged in to zoom using their email address. Activating this is recommended by zoom to prevent [Zoom Bombing](https://blog.zoom.us/wordpress/2020/03/20/keep-uninvited-guests-out-of-your-zoom-event/) but may impact registration due to its restrictive nature (forcing registrants to sign up for zoom before completing registration).
19. Check the `Breakout room pre-assign` if you’d like to pre-assign participants to the breakout sessions prior to the meeting. Please note that only participants/hosts with @gitlab email domain can be pre-assigned via the web interface. Participants with non @gitlab email addresses need to have zoom installed in their machine to be pre-assigned, and pre-assigning for this scenario can only be done through a CSV upload.
20. **(Recommended except for meetings that are private in nature)** Check `Record the meeting automatically` either on your local computer or in the cloud to automatically record the meeting when the host join.
21. Add alternate host as needed (need to be a Gitlab employee). For  presenters that are not a GitLab employee, ask them to register as a regular participant and grant them co-host access during the meeting.
22. SAVE your meeting.
23. :star2: **(Optional) Pro tip:** If you’d like to save the same  set up for a future meeting `Save your meeting as a Meeting template`.

**[Recommended for meetings requiring registration] Customize your registration form**

1. In the registration tab for the meeting that you have set up, click edit beside registration options.
2. Click the `Questions` Tab and select `Job Title` and `Country` (Do not select `Number of Employees` as we will add that as a custom field in the next step.
3. Click `Custom Questions` tab and click `New Question` button to add `Number of Employees` custom field.
    *  For `Type` select `Single answer`
    *  Make sure `Required` is checked
    *  In the ` Question` field,  fill in  `Number of Employees`
    *  In the answer field, fill in `1-99`, `100-499`, `500-1,999`, `2,000-9,999`, `10,000+`
    *  Click create
4. (Optional) Click `New Question` and follow the steps outlined previously to add more custom fields. Selecting type `Short Answer` vs. `Single Answer` will allow registrants to enter a free text response vs. selecting from a picklist.
5. Click `Save All`.

**[Recommended for meetings requiring registration] Customize your confirmation email & trigger a reminder email**

1. In the meeting that you have set up, click the `Email settings` tab.
2. Beside the `Confirmation Email to Registrants` section choose the email language (if not the default of `English`) , click edit and modify the editable `Subject`, `Body`, and `Footer` sections. Note: Zoom emails are text only and the layout is not editable.
3. **(Recommended) **Reminder emails have to be triggered manually for this set up. ±1 or 2 hour prior to the event, change the subject line of the Confirmation email to `[Meeting Topic] Confirmation/Reminder`, then click over to the `Registration tab`. In the `Registration tab` click `View` , select all the registrants on that page by selecting the top left checkbox and click `Resend Confirmation Email`. If the registration list spans across multiple pages you must repeat this for each page.

**[Optional for meetings requiring registration] Customize your landing page and email headers**

1. In the meeting that you have set up, click the `Branding` tab.
2. Upload your custom image to the `Banner` section. Image requirements are as follows:
    * GIF, JPG/JPEG or 24-bit PNG
    * The suggested dimensions: 640px by 200px
    * The maximum dimensions: 1280px by 400px

**[Optional] Add a poll to your meeting**

1. Make sure you have [enabled `Polling` in your account settings](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/meetings-with-without-breakout/index.html#prerequisites).
2. In the meeting that you have set up, click the `Poll` tab.
3. Click `Add`.
4. Put a descriptive title for the Poll.
5. Specify whether you want to allow the attendees to remain anonymous when submitting a poll.
6. Enter your poll question.
7. Specify if the answer will be a `Single choice` or `Multiple Choice` (allows for participant to select more than one answer).
8. `Save`.

#### During the event


**[Optional] Starting Livestream for your event**

1. Make sure you have [enabled `Livestream` in your account settings](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/meetings-with-without-breakout/index.html#prerequisites).
2. During the virtual event, follow this [step by step instructions](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/#livestream-with-zoom) to start LIVESTREAM.

**[Optional] Playing pre-recorded video during the meeting**
1. Make sure you have your video pulled up in a video player (quicktime/youtube) on your computer
2. On your Zoom panel, click share screen and select the video you want to share
3. At the bottom left of the pop up screen, select `Share computer sound` and `Optimize Screen Share for Video Clip`. Click `Share`.
4. Play the video.

Note: Although we can leverage this functionality for a semi-live virtual event, the audience will be able to tell that you are playing a video for them so it is good to share that information (be transparent) with them upfront.

#### Post event:

**[Optional] Downloading the registration list**

1. Login to zoom via the web interface.
2. On the left navigation, click `Reports`.
3. Click `Meeting`.
4. For` Report Type`, select `Registration Report`.
5. For `Search by time range` input the date of your event, click `Search`.
6. Select your event and click `Generate`
7. For `Registration Type` select `All Registrants`, click `Continue`.
8. Once the report finished processing, click `Download`.

**[Optional] Downloading the attendees list.**

1. Login to zoom via the web interface.
2. On the left navigation, click `Reports`.
3. Click `Usage`.
4. For `From` input the date of your event, click `Search`.
5. Find your event, scroll right to the `Participants` column, click the number of participants in blue fonts, select `Show unique users` checkbox on the top left of the pop up page, click `Export`.

