---
layout: handbook-page-toc
title: "SMB Account Executive"
---

**"You should expect excellence of yourself, teammates, and managers in your role. We will be excellent if we expect it." - Anonymous**
{: .text-center}

Small and Medium Size Business Account Executives "SMB AE" act as Account Executives and the face of GitLab for SMB prospects and customers. They are the primary point of contact for companies that employ between [1 to 99 employees](https://about.gitlab.com/handbook/business-ops/resources/#segmentation). These GitLab team members are responsible for working new business in their territory as well as handling the customer journey for new and existing customers. SMB Account Executives will assist prospects through their evaluation and buying process and will be the customer's point of contact for any renewal and expansion discussions.

## SMB Process 
### Tools
* [ZenDesk](https://about.gitlab.com/handbook/support/workflows/zendesk-overview.html) - Ticketing system
* [Version GitLab](https://version.gitlab.com/users/sign_in)- Customer Product Usage
* [Outreach](https://about.gitlab.com/handbook/marketing/marketing-operations/outreach/) - Email Sequencing 
* Salesforce - Customer Relationship Management 
* [LinkedIn Sales Navigator ](https://docs.google.com/document/d/1UF69ieck4AdHadzgPmZ5X1GBs3085JhlYaMowLj0AOg/edit) - Social Selling
* CaptivateIQ - Commission Calculator 
* [Clari](https://about.gitlab.com/handbook/sales/#clari-for-salespeople-instructional-videos) - Forecasting 
* [Datafox](https://about.gitlab.com/handbook/business-ops/tech-stack/#datafox) - Client Information
* [Customer Portal Admin](https://customers.gitlab.com/plans) 
* [Chorus](https://about.gitlab.com/handbook/business-ops/tech-stack/#chorus) (For NORAM SMB ONLY) - Call/Demo Recording 
*  [Crayon ](https://app.crayon.co/intel/gitlab/battlecards/) - Competitor Insights
*  [Periscope](https://app.periscopedata.com/app/gitlab/403199/Welcome-Dashboard-%F0%9F%91%8B) - Data Visualization
