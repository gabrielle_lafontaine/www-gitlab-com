---
layout: handbook-page-toc
title: "KPI Index"
---

## On this page
{:.no_toc .hidden-md .hidden-lg} 

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction
{:no_toc}
1. This page is the list of [KPIs](/handbook/ceo/kpis/) and links to the definitions in the handbook.
1. In order to maintain a [SSOT](/handbook/handbook-usage/#style-guide-and-information-architecture), KPI definitions live only in the functional section in the handbook, as close to the work as possible. So define performance indicators in the relevant page (for example the adoption page that also contains how the team works and has relevant videos) instead of a (K)PI page. 


## Updating

1. The Data Team is accountable for this page; therefore, please tag both Managers, Data on any MR updating information displaying on this page. 
1. If a KPI is not defined or is being updated, create a MR to propose or update a definition and ask the appropriate business stakeholder to review and merge.
1. If you remove, add, or change the essence of a KPI, the merge request needs to be approved by the function head and the CEO. Last person approving should merge the MR.
1. Tag the CFO; function head; both Managers, Data; and Manager, FP&A in the MR for visibility into downstream changes that may need to occur.
1. Once the MR is merged, the author makes an announcement on the Company Call, along with the link to the Merge Request.
1. FP&A will add an * for the definition change to the Investor Metrics along with a link to the Merge Request that describes the change.
1. If the KPI being changed is one of the following, the CFO will need to notify investors and any relevant external parties:

    - [Net Retention](/handbook/customer-success/vision/#retention-and-reasons-for-churn)
    - [Gross Retention](/handbook/customer-success/vision/#retention-and-reasons-for-churn)
    - [ARR](/handbook/sales/#annual-recurring-revenue-arr)


## Discussion about page improvements
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/R1F-Iup-5Tc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Progress

There are two goals that are tracked on this page: 
*  [Phase 1](/handbook/business-ops/data-team/kpi-index/#phase-1) and 
*  [Phase 2](/handbook/business-ops/data-team/kpi-index/#phase-2)

### Phase 1 

The goal of Phase 1 is for each KPI to be operational in any system with a link to that data visualization or as an embedded chart in the handbook. 


### Phase 2 

The goal of Phase 2 is to embed a Sisense chart for each KPI that can be operationalized. 
KPI must be [fully defined](/handbook/ceo/kpis/#parts-of-a-kpi) with the data source available in the Data warehouse for the Data Team to prioritize the KPI. 
This dashboard shares the progress of operationalizing KPIs in Sisense: 

<iframe class="dashboard-embed" src="https://app.periscopedata.com/shared/5890b9a4-8608-4a82-a23d-635af1ad0529?embed=true" height="700"> </iframe>

## Legend 

The icons below are relevant for [Phase 1](/handbook/business-ops/data-team/kpi-index/#phase-1) and can be assigned by anyone at GitLab. 

📊 indicates that the KPI is operational and is embedded in the handbook next to the definition and shown publicly (can be Sisense or another system).

🔗 indicates that the KPI is operational and there is a link from the handbook to Sisense, GitLab, Bitergia, Grafana, or another system.
This may be because the KPI cannot be public or because it isn't possible to embed it yet. 

🚧 indicates that the KPI is in a `WIP: work in progress` status estimated to be shipped in any system within the month. When using this indicator, an issue should also be linked from this page.

🐔 indicates that the KPI is unlikely to be operationalized in the next month.


## GitLab KPIs

GitLab KPIs are duplicates of goals of the reports further down this page.  
GitLab KPIs are the most important indicators of company performance, and the most important KPI is IACV.

1. [IACV](/handbook/sales/#incremental-annual-contract-value-iacv) vs. plan > 1  [🔗](https://app.periscopedata.com/app/gitlab/446004/Sales-KPIs?widget=6933430&udv=0) (lagging)
1. [Capital Consumption < Plan](/handbook/finance/corporate-finance-performance-indicators/#capital-consumption-vs-plan) [🔗](https://app.periscopedata.com/app/gitlab/483606/Finance-KPIs?widget=6279480&udv=0) (lagging)
1. [IACV efficiency](/handbook/sales/#iacv-efficiency-ratio) > 1 [📊](https://app.periscopedata.com/app/gitlab/446004/Sales-KPIs?widget=6912064&udv=0) (lagging)
1. [Net New Business Pipeline Created ($s)](/handbook/marketing/revenue-marketing/#revenue-marketing-kpi-definitions) v Plan > 1  [📊](https://app.periscopedata.com/app/gitlab/431555/Marketing-Metrics?widget=6592070&udv=0) (leading for IACV)
1. [Wider community merged MRs per release](/handbook/marketing/community-relations/code-contributor-program/#wider-community-contributions-per-milestone) [📊](https://app.periscopedata.com/app/gitlab/593556/Wider-Community-Dashboard?widget=7765561&udv=0) (lagging)
1. [LTV / CAC ratio](/handbook/sales/index.html#life-time-value-to-customer-acquisition-cost-ratio-ltvcac) > 4 🐔(lagging)
1. [Interviewee Satisfaction (ISAT)](/handbook/hiring/performance_indicators/#interviewee-satisfaction-isat) > 4.1 [📊](https://app.periscopedata.com/app/gitlab/482006/People-KPIs?widget=6858936&udv=820057) (lagging)
1. [Hires vs. plan](/handbook/hiring/performance_indicators/#hires-vs-plan) > 0.9 [📊](https://app.periscopedata.com/app/gitlab/482006/People-KPIs?widget=6888845&udv=820057)
1. [12 month team member retention](/handbook/people-group/people-group-metrics/#team-member-retention) > 84% 🐔 (lagging)
1. [Merge Requests per release per engineer in product development](/handbook/engineering/development/performance-indicators/#mr-rate) > 10 [📊](https://app.periscopedata.com/app/gitlab/504639/Development-KPIs?widget=6946736) (lagging)
1. [GitLab.com Availability](/handbook/engineering/infrastructure/performance-indicators/#gitlab-com-availability) > 99.95% [🔗](https://dashboards.gitlab.com/d/GTp20b1Zk/public-dashboard-splashscreen?orgId=1&from=now-30d&to=now) (lagging)
1. [SMAU](/handbook/product/metrics/#stage-monthly-active-users-smau) [🚧](https://gitlab.com/gitlab-data/analytics/-/issues/3840) (lagging)
1. [Support Satisfaction](/handbook/support/performance-indicators/#support-satisfaction-ssat) [📊](https://app.periscopedata.com/app/gitlab/463858/Engineering-KPIs?widget=5992548) (lagging)
1. [Runway](/handbook/finance/corporate-finance-performance-indicators/index.html#runway) > 12 [🔗](https://app.periscopedata.com/app/gitlab/483606/Finance-KPIs?widget=6880820&udv=0) (lagging)
1. [Product Downloads](/handbook/alliances/#product-download-by-distribution-method) [📊](https://app.periscopedata.com/app/gitlab/428908/GitLab-Downloads-by-Installation-Type?widget=5542406&udv=0) (leading for usage)

## Sales KPIs

1. [IACV](/handbook/sales/#incremental-annual-contract-value-iacv) vs. plan > 1  [🔗](https://app.periscopedata.com/app/gitlab/446004/Sales-KPIs?widget=6933430&udv=0)
1. [Field efficiency ratio](/handbook/sales/#field-efficiency-ratio) > 2 [📊](https://app.periscopedata.com/app/gitlab/446004/Sales-KPIs?widget=6104902&udv=0)
1. [IACV efficiency](/handbook/sales/#iacv-efficiency-ratio) > 1 [📊](https://app.periscopedata.com/app/gitlab/446004/Sales-KPIs?widget=6912064&udv=0)
1. [TCV](/handbook/sales/#total-contract-value-tcv) vs. plan > 1 [🔗](https://app.periscopedata.com/app/gitlab/446004/Sales-KPIs?widget=6922179&udv=0)
1. [ARR](/handbook/sales/#annual-recurring-revenue-arr) YoY > 190% [🔗](https://app.periscopedata.com/app/gitlab/446004/Sales-KPIs?widget=6933786&udv=0)
1. [Win rate](/handbook/sales/#win-rate) > 30% [📊](https://app.periscopedata.com/app/gitlab/446004/Sales-KPIs?widget=6138977&udv=0)
1. % of ramped reps at or above quota > 70% 🐔
1. % of ramping reps (< 1 year) at or above 70% of quota > 80% 🐔
1. [Net Retention](/handbook/customer-success/vision/#retention-and-reasons-for-churn) > 180%  [🔗](https://app.periscopedata.com/app/gitlab/446004/Sales-KPIs?widget=6055135&udv=0)
1. [Gross Retention](/handbook/customer-success/vision/#retention-and-reasons-for-churn) > 90% [🔗](https://app.periscopedata.com/app/gitlab/446004/Sales-KPIs?widget=6062086&udv=0)
1. Rep [IACV  per comp](/handbook/sales/#measuring-sales-rep-productivity) > 5 🐔
1. [ProServe](/handbook/sales/#pcv) revenue vs. cost > 1.1 [📊](https://app.periscopedata.com/app/gitlab/446004/Sales-KPIs?widget=6931495&udv=0)
1. [Services attach rate for strategic](/handbook/customer-success/professional-services-engineering/#services-attach-rate-for-large-accounts) > 80% [📊](https://app.periscopedata.com/app/gitlab/446004/Sales-KPIs?widget=6202331&udv=0)
1. [Self-serve sales ratio](/handbook/sales/#self-serve-sales-ratio) > 30% [📊](https://app.periscopedata.com/app/gitlab/446004/Sales-KPIs?widget=6168683&udv=0)
1. [Licensed users](/handbook/sales/#licensed-users) [📊](https://app.periscopedata.com/app/gitlab/446004/Sales-KPIs?widget=6936917&udv=0)
1. [ARPU](/handbook/sales/#revenue-per-licensed-user-also-known-as-arpu) [🔗](https://app.periscopedata.com/app/gitlab/446004/Sales-KPIs?widget=6936923&udv=0)
1. [Active SMAU](/handbook/product/metrics/#stage-monthly-active-users-smau) for Paying Customers 🐔
1. [New hire location factor](/handbook/hiring/metrics/#new-hire-location-factor) < 0.72 [📊](https://app.periscopedata.com/shared/cfbba3f6-0c48-4946-9d00-b5b20a39907a?)


## Marketing KPIs

1. [Net New Business Pipeline Created ($s)](/handbook/marketing/revenue-marketing/#revenue-marketing-kpi-definitions) v Plan > 1  [📊](https://app.periscopedata.com/app/gitlab/431555/Marketing-Metrics?widget=6592070&udv=0)
1. [Pipe-to-spend per marketing activity](/handbook/marketing/revenue-marketing/#revenue-marketing-kpi-definitions) > 5 🐔
1. Marketing efficiency ratio > 2 🐔
1. [Opportunities per XDR per month created](/handbook/marketing/revenue-marketing/#revenue-marketing-kpi-definitions)  [📊](https://app.periscopedata.com/app/gitlab/431555/Marketing-Metrics?widget=6222245&udv=0)
1. [LTV / CAC ratio](/handbook/sales/index.html#life-time-value-to-customer-acquisition-cost-ratio-ltvcac) > 4 🐔
1. [Social Media Followers](/handbook/marketing/corporate-marketing/social-marketing/) 🐔  
1. [New Unique Web Visitors (about.gitlab.com)](/handbook/marketing/revenue-marketing/digital-marketing-programs/#digital-marketing-kpis) 🐔
1. [Total Web Sessions (about.gitlab.com)](/handbook/marketing/revenue-marketing/digital-marketing-programs/#digital-marketing-kpis) 🐔
1. [Meetups per month](/handbook/marketing/community-relations/evangelist-program/#gitlab-meetups-per-month-kpi-definition) [📊](https://app.periscopedata.com/app/gitlab/431555/Marketing-Metrics?widget=7799761&udv=0)
1. [Wider Community merged MRs per release](/handbook/marketing/community-relations/code-contributor-program/#wider-community-contributions-per-milestone) [📊](https://app.periscopedata.com/app/gitlab/593556/Wider-Community-Dashboard?widget=7765561&udv=0)
1. [Community Channel Response Time](/handbook/marketing/community-relations/community-advocacy/#social-response-time-kpi-definition) 🐔
1. [New hire location factor](/handbook/hiring/metrics/#new-hire-location-factor) < 0.72 [📊](https://app.periscopedata.com/shared/65b5b2a9-83bd-4af4-bfc3-5ae9ee632af3?)
1. [Pipeline coverage: 2X for current quarter, 1X for next quarter, and .5 for 2 QTRs out](/handbook/sales/#pipeline-coverage-ratio) 🐔
1. [Total # of MQLs by month](/handbook/marketing/revenue-marketing/#revenue-marketing-kpi-definitions) [📊](https://app.periscopedata.com/app/gitlab/431555/Marketing-Metrics?widget=6592066&udv=0)
1. [Lead follow-up response time](/handbook/marketing/revenue-marketing/#revenue-marketing-kpi-definitions) 🐔
1. Qty. of Case Studies published per month 🐔
1. Larger SOV compared to most published competitor 🐔
1. Growth in amount of partnership focused media coverage 🐔
1. Growth in amount of remote work focused media coverage 🐔
1. [Product Downloads](/handbook/alliances/#product-download-by-distribution-method) [📊](https://app.periscopedata.com/app/gitlab/428908/GitLab-Downloads-by-Installation-Type?widget=5542406&udv=0)

## People Group KPIs

1. [Hires vs. plan](/handbook/hiring/performance_indicators/#hires-vs-plan) > 0.9 [📊](https://app.periscopedata.com/app/gitlab/482006/People-KPIs?widget=6888845&udv=820057)
1. [Time to Offer Accept (Days)](/handbook/hiring/performance_indicators/#time-to-offer-accept-days) < 45 [📊](https://app.periscopedata.com/app/gitlab/482006/People-KPIs?widget=6235296&udv=820057)
1. [Offer acceptance rate](/handbook/hiring/performance_indicators/#offer-acceptance-rate) > 0.9 [📊](https://app.periscopedata.com/app/gitlab/482006/People-KPIs?widget=6235279&udv=820057)
1. [Interviewee Satisfaction (ISAT)](/handbook/hiring/performance_indicators/#interviewee-satisfaction-isat) > 4.1 [📊](https://app.periscopedata.com/app/gitlab/482006/People-KPIs?widget=6858936&udv=820057)
1. [Average location factor](/handbook/people-group/people-group-metrics/#average-location-factor) < 0.65 [📊](https://app.periscopedata.com/app/gitlab/482006/People-KPIs?widget=6238450&udv=820057)
1. [Percent of team members over compensation band](/handbook/people-group/people-group-metrics/#percent-over-compensation-band) <= 1% 🐔
1. [New hire location factor](/handbook/hiring/metrics/#new-hire-location-factor) < 0.72 [📊](https://app.periscopedata.com/shared/4daa9bd3-8f26-46b7-a50d-24041ca801fa?)
1. [12 month team member retention](/handbook/people-group/people-group-metrics/#team-member-retention) > 84% 🐔
1. [12 month voluntary team member turnover](/handbook/people-group/people-group-metrics/#team-member-voluntary-turnover) < 10% [📊](https://app.periscopedata.com/app/gitlab/482006/People-KPIs?widget=6873851&udv=820057)
1. [Candidates Sourced by Recruiting Department versus Candidates Hired](/handbook/hiring/performance_indicators/#candidates-sourced-by-recruiting-department-vs-candidates-hired) > 0.28 🐔
1. [Onboarding Satisfaction (OSAT)](/handbook/people-group/people-group-metrics/#onboarding-satisfaction-osat) > 4 [📊](https://app.periscopedata.com/app/gitlab/482006/People-KPIs?widget=6873595&udv=820057)
1. [Diversity lifecycle: applications, recruited, interviews, offers, acceptance, retention](/company/culture/inclusion/#performance-indicators) 🐔
1. [Spend per team member](/handbook/people-group/people-group-metrics/#spend-per-team-member) 🐔
1. [Discretionary bonuses](/handbook/incentives/#discretionary-bonuses) per team member per month > 0.1 [📊](https://app.periscopedata.com/app/gitlab/482006/People-KPIs?widget=6251794&udv=820057)
1. [Percent of sent Slack messages that are not DMs](/handbook/communication/#why-we-track--of-messages-that-are-not-dms) > 50% [📊](https://app.periscopedata.com/app/gitlab/513609/GitLab-Slack?widget=6849380&udv=0)
1. [Cost Per Hire](/handbook/hiring/metrics/#cost-per-hire) < $6,500 [🔗](https://app.periscopedata.com/app/gitlab/482006/People-KPIs?widget=6946649&udv=841419)

### Recruiting Department KPIs

<%= kpi_list_by_org("Recruiting") %>

## Finance KPIs

### Corporate Financial KPIs

<%= kpi_list_by_org("Corporate Finance") %>

### Finance Team KPIs

1. [Average days to close](/handbook/finance/accounting/#average-days-to-close-kpi-definition) < 10 [📊](https://app.periscopedata.com/app/gitlab/483606/Finance-KPIs?widget=6500157&udv=0)
1. [New hire location factor](/handbook/hiring/metrics/#new-hire-location-factor) < 0.69 [📊](https://app.periscopedata.com/shared/7e3743f8-48bd-41f6-9173-44d904274ad6?)
1. [Scalable Employment Solution](/handbook/tax/performance-indicators/#international-expansion) >99% [🚧](https://gitlab.com/gitlab-data/analytics/-/issues/3788)
1. [Deliver Quantified Savings of](/handbook/finance/procurement/#procurement-kpi) > > $3,000,000 over a rolling 12 month period 🐔


## Product KPIs

<%= kpi_list_by_org("Product") %>

## Product Strategy KPIs

1. [Acquisition velocity](/handbook/acquisitions/performance-indicators/#acquisition-velocity) > 10 per year 🐔
1. [Acquisition success](/handbook/acquisitions/performance-indicators/#acquisition-success) > 70% 🐔
1. [Qualified acquisition targets](/handbook/acquisitions/performance-indicators/#qualified-acquisition-targets) > 1000 per year 🐔
1. [New hire location factor](/handbook/hiring/metrics/#new-hire-location-factor) < 0.7 [📊](https://app.periscopedata.com/shared/664db918-e9eb-4fc8-94fa-6ef0934cc8dc?)

## Engineering KPIs

<%= kpi_list_by_org("Engineering Function") %>

### Development Department KPIs

<%= kpi_list_by_org("Development Department") %>

### Infrastructure Department KPIs

<%= kpi_list_by_org("Infrastructure Department") %>

### Quality Department KPIs

<%= kpi_list_by_org("Quality Department") %>

### Security Department KPIs

<%= kpi_list_by_org("Security Department") %>

### Support Department KPIs

<%= kpi_list_by_org("Support Department") %>

### UX Department KPIs

<%= kpi_list_by_org("UX Department") %>

## GitLab Metrics

We share a spreadsheet with investors called "GitLab Metrics."
The GitLab Metrics Sheet should be a subset of the KPIs listed on this page.
Alternatively, the sheet may show variations or subsets of one of those KPIs, such as showing all licensed users and then licensed users by product.

## Satisfaction

We do satisfaction scope on a scale of 1 to 5 how satisfied people are with the experience.
We don’t use NPS since that cuts off certain scores and we want to preserve fidelity.
We have the following abbreviation letter before SAT, please don’t use SAT without letter before to specify it:

- C = unused since customer is ambiguous (can mean product or support, not all users are customers)
- E = unused since employee is used by other companies but not by us
- I = [Interviewee](/handbook/hiring/metrics/#interviewee-satisfaction-isat) (would you recommend applying here)
- L = [Leadership](/handbook/eba/#leadership-sat-survey) (as an executive with dedicated administrative support, how is your executive administrative support received)
- O = [Onboarding](/handbook/people-group/people-operations-metrics/#onboarding-tsat) (how was your onboarding experience)
- P = [Product](/handbook/product/metrics/#paid-net-promoter-score) (would you recommend GitLab the product)
- S = [Support](/handbook/support/#support-satisfaction-ssat) (would you recommend our support followup)
- T = Team-members (would you recommend working here)

## Retention

Since we track retention in a lot of ways, we should never refer to just "Retention" without indicating what kind of retention.
We track:

* [Net Retention](/handbook/customer-success/vision/#retention-and-reasons-for-churn)
* [Gross Retention](/handbook/customer-success/vision/#retention-and-reasons-for-churn)
* User Retention
* [Team Member Retention](/handbook/people-group/people-group-metrics/#team-member-retention)

## Working with the Data Team

Please see [the Data Team Handbook](/handbook/business-ops/data-team/).
