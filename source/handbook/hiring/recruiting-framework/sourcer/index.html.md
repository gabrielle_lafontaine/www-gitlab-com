---
layout: handbook-page-toc
title: "Recruiting Process - Sourcer Tasks"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Recruiting Process Framework - Sourcer Tasks
{: #framework-source}

### Source top talent, schedule screens

The purpose of sourcing is to generate a large list of qualified potential 
candidates, that will allow us to grow our company and build an even more diverse team in the most efficient way possible, while also sustaining our culture. 

### Sourcer and Recruiter: Define Sourcing Priorities

Even if a role doesn't require continious sourcing effort, our goal is to ensure  that 
we look for outbound talent and leverage our sourcing effort for all the roles 
we’re hiring for.

Sourcer and Recruiter should closely work with their Hiring Leaders to define Sourcing Priority (P0, P1, P2) for roles they are working on and strategize their sourcing effort.
Every role should receive sourcing support based on the [Recruiting Alingmnent](/handbook/hiring/recruiting-alignment/).

### Recruiter: Schedule and complete Intake

As a common practice, Recruiter will schedule an intake call and invite their Sourcing partner. 
Sourcer should come prepared for the intake session with insights and required questions to 
serve as a talent advisor. They can use the [Req Intake template](https://gitlab.com/gl-recruiting/req-intake) to run this meeting.

During the intake session all the participants should agree on the sourcing priority.
If a role doesn't require constant sourcing support, Sourcer will schedule a Sourcing Session to make sure that we consider outbound talent for every opening at GitLab.

To stay up to date, Sourcer should have regular check-ins with the Recruiter and
Hiring Manger.

Refer to [Step 5/R: Schedule intake](/handbook/hiring/recruiting-framework/recruiter/#step-5r-schedule-intake) and [Step 6/R: Complete Intake](/handbook/hiring/recruiting-framework/recruiter/#step-6r-complete-intake) in recruiting 
framework for detailed information on how to schedule and complete intake.

### Sourcer: Schedule Sourcing Session

Once a new role is opened, Sourcer and Recruiter will discuss with Hiring Manager who should participate in the Sourcing session. The Sourcing Session is focused on finding outbound candidates outside of our Talent Community.

Our recommendation is to have at least Hiring Manager and Interviewers participating in a 
Sourcing Session but if the team is too busy and can’t contribute to the sourcing effort, 
a Sourcing Session should be handled only by Sourcer or by Sourcer and Recruiter with the majority of 
prospects expected from Sourcer.

A Sourcing Session should be scheduled within 5 business days after the intake call. 
For the “ever-green” and “volume” roles we recommend having regular Sourcing Sessions every month.

All Sourcing sessions that include team members outside of the Recruiting team should be set up for 30 minutes by default and could be done 
live or in an async way. 
 
We don’t require a specific number of profiles to be added by every participant, 
however, Sourcer should come up with at least 20 qualified profiles for the role.
Sourcer will reach out to all the candidates that are added during the Sourcing Session
within 2 business days and add their profiles to Greenhouse.

Sourcer assigned to the role is responsible for all the further updates and status changes in Greenhouse.

### Sourcer: Source candidates 

Sourcer uses different sourcing avenues to find candidates, with LinkedIn and GitLab Talent Community being the most 
used and efficient ones.  
Ensure you have a LinkedIn Recruiter account and your ContactOut (email finder plugin) is installed.  
The access to GitLab Talent Community should be granted to every new Recruiting team member during their onboarding.

For additional details/information refer to our [Sourcing Handbook page.](/handbook/hiring/sourcing/)

### Sourcer: Reach Out

Sourcers are responsible for reaching out to candidates. They can use our 
[reach out templates](https://docs.google.com/presentation/d/1ySqgLoYnFUGtb7hdywav6iSb_NBPRhfIs6WZlGne6Ww/edit?usp=sharing) or create their own messaging for reaching out to candidates. 

### Sourcer: Enable Greenhouse Integration 

Greenhouse is the only source of truth. Ensure your LinkedIn account is 
connected to Greenhouse to export candidates in one click as prospects 
([Enable LinkedIn Recruiter System Connect (RSC)](https://support.greenhouse.io/hc/en-us/articles/115005678103-Enable-LinkedIn-Recruiter-System-Connect-RSC-)).

### Sourcer: Add prospects to Greenhouse

All approached candidates should be added in Greenhouse as prospects. Sourcer should make sure that every sourced candidate has their Sourcer's name in the Source field.
If a Prospect was sourced by a Recruiting Team Member from the Talent Community, that user will need to [manually update the candidate's source](https://about.gitlab.com/handbook/hiring/greenhouse/#high-level-workflow) information.
There are multiple ways to add the candidates, as follows: 

* [LinkedIn](https://support.greenhouse.io/hc/en-us/articles/204110135-Add-Prospects-to-Greenhouse-via-LinkedIn-RSC-Integration)  
* [Greenhouse plug-in](https://support.greenhouse.io/hc/en-us/articles/201444934-Prospecting-with-Greenhouse-Prospecting-Google-Chrome-Plugin) 
* [Maildrop](https://support.greenhouse.io/hc/en-us/articles/201990630)

### Sourcer: Scheduling interviews in Greenhouse

As per the interview process, schedule the candidates for next steps (leave a note in Greenhouse to recruiter and CES team member with an update). For Example:- Schedule a screening call with a candidate through Greenhouse or using recruiters’s calendly link.

* [Convert Prospect into Candidate](https://support.greenhouse.io/hc/en-us/articles/360025848912-Convert-Prospect-into-Candidate)
* [Google Calendar](https://support.greenhouse.io/hc/en-us/articles/360022018271-Schedule-Interview-to-Own-Calendar-Google-Calendar-)
* [Calendly](https://support.greenhouse.io/hc/en-us/articles/360029686091-Schedule-Interview-With-Calendly)

[World Clock Converter](https://www.worldtimebuddy.com/) - Find the exact time difference for multiple Time Zones.  

### Sourcer: Use Greenhouse for tracking your prospects

* Use the [Follow](https://support.greenhouse.io/hc/en-us/articles/203800369-Follow-Email-Notifications-About-Candidate-or-Prospect) button to get timely updates on the candidate's status. 
* [Follow-up Reminders](https://support.greenhouse.io/hc/en-us/articles/360017456592-Follow-up-Reminders) to resend the assessment, availability or for any other important updates.  
* [Tags](https://support.greenhouse.io/hc/en-us/articles/360027904392-Assign-Candidate-Tag-): The most common tags used are “Location sourcing” and “diversity sourcing”. You can also create your [auto tags](https://support.greenhouse.io/hc/en-us/articles/360000060551)
* [Greenhouse Candidates Filter](https://support.greenhouse.io/hc/en-us/articles/360004175751-Best-Practices-Sourcing-Strategies): Similar to LinkedIn, Greenhouse has filters to track your pipeline. Under the candidates section, you can check any team members and your candidates tagged under a specific job, interview stage, location and so on. The most commonly used are Source, Profile details, Jobs and Location. Example - [Filter Candidates by Custom Job Fields](https://support.greenhouse.io/hc/en-us/articles/360003493951-Filter-Candidates-by-Custom-Job-Fields)

For more information about Greenhouse and it’s tips & tricks, please refer to this [document](https://docs.google.com/document/d/1BbO5v_IJEq4QR9KpI7T3fSCwdCapVOZCyNgEk6MYO0s/edit)






