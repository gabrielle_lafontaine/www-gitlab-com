---
layout: markdown_page
title: Product Direction - Retention
---

### Overview

The Retention Team at GitLab focuses on uncovering and addressing the leading reasons for subscription cancellation through hypothesis based testing and iteration. We will analyze churn from both a revenue and net customer perspective and will use that information to inform prioritization. 

We will gain a deep understanding of the top reasons for churn by leveraging qualitative data from our customers and by partnering with Customer Success and Customer Support. In addition to the qualitative data we will analyze quantitative data to understand the key signals that indicate an account is at-risk or successful. 

Our testing will likely focus on identifying at-risk customers and guiding them back to successful. 

Our goal is to keep teams happy and engaged with GitLab so they can continue to realze and benefit from the value that GitLab provides.  

**Retention Team**

Product Manager: [Michael Karampalas](/company/team/#mkarampalas) | 
Engineering Manager: [Phil Calder](/company/team/#pcalder) | 
UX Manager: [Jacki Bauer](/company/team/#jackib) | 
Product Designer: [Tim Noah](/company/team/#timnoah) | 
Full Stack Engineer: [Jeremy Jackson](/company/team/#jejacks0n) | 
Full Stack Engineer: [Jay Swain](/company/team/#jayswain)

**Retention Team Mission**

Encourage customers to keep using GitLab by: 
1. Providing a simple, easy, and transparent renewal and true-up process that can be both flexible for our larger customers and automated for our smaller ones
1. Identifying and coaching at-risk users by partnering with our Customer Success team 
1. Helping our users realize and experience the full value of the GitLab offering 

**Retention KPI**

Gross Retention

Gross retention is defined as:
Gross Retention (%) = (min(B, A) / A) * 100%

* A. MRR 12 months ago from currently active customers
* B. Current MRR from the same set of customers as A.


_Supporting performance indicators:_

* 30/90/180/360 day MAU retention
* % of subscriptions with auto-renew enabled 
* Won renewal ACV
* Lost renewal ACV
* Renewal rate: The % of subscriptions or licenses that are renewed for a given cohort. 

### Problems to solve

Do you have challenges? We’d love to hear about them, how can we help GitLab contributors stay happy and engaged with GitLab? 

* The renewal process is confusing and clunky. We need to make it easy for our self-hosted and gitlab.com users to renew. 
* There are a lot of manual processes and effort involved in our renewals, including accounts that use "auto-renew". We need to streamline and automate as much of the renewal process as possible so that our GitLab team members can focus their time and energy on making our customers successful.
* We have users that disengage with our product for various reasons. We need to gain a better understanding of those reasons and work in conjunction with Customer Success to help get those users back on track and experiencing the full value of the GitLab offering. 


### Our approach

Retention runs a standard growth process. We gather feedback, analyze the information, ideate, then create experiments and test. As you can imagine the amount of ideas we can come up with is enormous so we use the [ICE framework](https://blog.growthhackers.com/the-practical-advantage-of-the-ice-score-as-a-test-prioritization-framework-cdd5f0808d64) (impact, confidence, effort) to ensure we are focusing on the experiments that will provide the most value. 


### Maturity

The growth team at GitLab is new and we are iterating along the way. As of August 2019 we have just formed the Retention group and are planning to become a more mature, organized, and well oiled machine by January 2020. 


| Category | Description |
| ------ |  ------ |
| Paid account signup | How our users purchase GitLab Plans and add-ons when initialy creating their GitLab account. |



### Current Priorities

<table>
  <tr>
   <td>ICE Score
   </td>
   <td>Focus
   </td>
   <td>Why/Hypothesis
   </td>
   <td> Status
   </td> 
  </tr>
  <tr>
   <td>9.0
   </td>
   <td><a href="https://gitlab.com/gitlab-org/growth/product/issues/43">Disable ability to turn Auto-Renew off for .com and provide a cancel option</a>
   </td>
   <td>.com customers are not aware that disabling auto-renew is essentially the same thing as canceling. If we allow users to cancel (e.g. turn auto-renew off) and explain what they'll lose, we'll see a higher % of subscriptions renew. By collecting feedback during the cancellation process we will also learn of additional opportunities to increase retention. 
   </td>
   <td> <a href="https://gitlab.com/gitlab-org/growth/product/issues/162">AB test a success, pushed to 100%</a>
   </td> 
  </tr>
  <tr>
   <td>8.0
   </td>
   <td><a href="https://gitlab.com/gitlab-org/growth/engineering/issues/42">Charge for `seats currently in use` on GitLab.com when Auto Renew is enabled</a>
   </td>
   <td>When a customer's subscription has the Auto Renew setting toggled to ON, the system will create a new subscription at the renewal date identical to the existing subscription. The problem is, we are losing license money where there are more seats active on GitLab.com than what was in the subscription.
   </td>
   <td> Completed 
   </td>
  </tr>
  <tr>
   <td>7.3
   </td>
   <td><a href="https://gitlab.com/groups/gitlab-org/growth/-/epics/3">Provide better messaging and context in the renewal banners</a>
   </td>
   <td>The current renewal  banners have a number of problems. This work will address the ability to dismiss the banner, linking customers to the portal, handling auto-renew with different messaging, and improving the look and feel of the banner.
   </td>
   <td>In progress - some pieces delivering in 12.10 
   </td>
  </tr>
  <tr>
   <td> 7.0
   </td>
   <td><a href="https://gitlab.com/gitlab-org/growth/product/-/issues/1486">Add and Update Renewal Notifications</a>
   </td>
   <td>We currently only send one renewal notification to customers 30 days before their renewal. Some customers require more advanced notice. This change adds multiple notifications over a 90 day window with customized instructions that are specific to the customer and their subscription.  
   </td> 
   <td> Completed 
   </td>
  </tr>
  <tr>
  <td> 6.5
   </td>
   <td><a href="https://gitlab.com/gitlab-org/growth/product/issues/279">What's New Notification</a>
   </td>
   <td>We believe that by surfacing a notification icon/interaction within gitlab which highlights our recent product enhancements, we'll be able to drive additional awareness and engagement with new features, leading to more engaged accounts and increased retention.
   </td> 
   <td> MVC releasing for GitLab.com 
   </td>
  </tr>
</table>







### Helpful Links

[GitLab Growth project](https://gitlab.com/gitlab-org/growth)

[KPIs & Performance Indicators](https://about.gitlab.com/handbook/product/metrics/)

